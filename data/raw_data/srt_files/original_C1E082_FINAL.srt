1
00:00:00,000 --> 00:00:01,680
 You know what I mean?

2
00:00:01,680 --> 00:00:02,350
 I don't know.

3
00:00:02,350 --> 00:00:02,566
 I don't think--
this is a bad idea.

4
00:00:02,566 --> 00:00:03,280
 Why can't I just--

5
00:00:03,280 --> 00:00:05,488
 Vex, Vax, and Keyleth--
as you guys are having this

6
00:00:05,488 --> 00:00:08,050
conversation, something
catches the corner of your eye,

7
00:00:08,050 --> 00:00:10,010
and you watch something just go
[WHOOSHING NOISE] and just whiz

8
00:00:10,010 --> 00:00:10,635
past.

9
00:00:10,635 --> 00:00:10,910
 Oh, shit.

10
00:00:10,910 --> 00:00:13,035
 Like from in the bottom
of the ravine to the top.

11
00:00:13,035 --> 00:00:13,630
Just--

12
00:00:13,630 --> 00:00:14,130
 Like what?

13
00:00:14,130 --> 00:00:14,790
 We saw that.

14
00:00:14,790 --> 00:00:15,450
We saw that.

15
00:00:15,450 --> 00:00:17,074
 You saw something
quickly just go by.

16
00:00:17,074 --> 00:00:18,317
 Can I get a better look?

17
00:00:18,317 --> 00:00:19,150
Can I look and see--

18
00:00:19,150 --> 00:00:19,370
 Can we, after the fact--

19
00:00:19,370 --> 00:00:19,690
 --if I see it again?

20
00:00:19,690 --> 00:00:20,460
 --try to figure
out what we see?

21
00:00:20,460 --> 00:00:20,890
 Do we roll alone?

22
00:00:20,890 --> 00:00:21,160
 Yeah.

23
00:00:21,160 --> 00:00:22,020
The three of you guys
make a perception check,

24
00:00:22,020 --> 00:00:24,036
as you kind of come up
to the edge and look.

25
00:00:24,036 --> 00:00:25,280
 Oh, shit.

26
00:00:25,280 --> 00:00:26,820
 26.

27
00:00:26,820 --> 00:00:27,380
 14.

28
00:00:27,380 --> 00:00:29,250
 Is this related to dragons?

29
00:00:29,250 --> 00:00:31,010
 This is not
related to dragons.

30
00:00:31,010 --> 00:00:32,450
 Ha, ha, ha.

31
00:00:32,450 --> 00:00:38,070
 That is a 21.

32
00:00:38,070 --> 00:00:39,270
 26.

33
00:00:39,270 --> 00:00:40,107
 OK.

34
00:00:40,107 --> 00:00:40,690
 Best we got.

35
00:00:40,690 --> 00:00:41,190
 All right.

36
00:00:41,190 --> 00:00:45,430
So you're kind of
seeing little things--

37
00:00:45,430 --> 00:00:49,020
just right out of the
middle of the light,

38
00:00:49,020 --> 00:00:53,622
you can see just something
shifting above and below you.

39
00:00:53,622 --> 00:00:55,080
You guys have a
little better view.

40
00:00:55,080 --> 00:00:56,620
You still can't quite see.

41
00:00:56,620 --> 00:01:00,360
The length of your
darkvision is too far for you

42
00:01:00,360 --> 00:01:02,531
to see what is below you,
or initially what's above.

43
00:01:02,531 --> 00:01:03,030
But you--

44
00:01:03,030 --> 00:01:06,540
 Can I-- as a quick reaction--
grab the rope bridge?

45
00:01:06,540 --> 00:01:07,100
 Yeah.

46
00:01:07,100 --> 00:01:08,004
 OK.

47
00:01:08,004 --> 00:01:09,420
 So you're still
off to the side,

48
00:01:09,420 --> 00:01:10,020
just holding on to the bridge?

49
00:01:10,020 --> 00:01:10,920
 Grasping, yeah.

50
00:01:10,920 --> 00:01:11,620
 OK.

51
00:01:11,620 --> 00:01:14,098
You are being watched.

52
00:01:14,098 --> 00:01:21,280
[BLOWING NOISE] What
looks to be figures--

53
00:01:21,280 --> 00:01:22,937
humanoid bodies.

54
00:01:22,937 --> 00:01:23,853
 Oh, I have a theory.

55
00:01:23,853 --> 00:01:24,686
 Are those spirits?

56
00:01:24,686 --> 00:01:26,228
 They're just kind
of tumbling past.

57
00:01:26,228 --> 00:01:28,269
One of them kind of hits
the edge of the bridge--

58
00:01:28,269 --> 00:01:29,970
[CRACKING NOISE] and
keeps spinning off,

59
00:01:29,970 --> 00:01:31,200
and just disappears
in the darkness above.

60
00:01:31,200 --> 00:01:32,320
 I have a weird theory.

61
00:01:32,320 --> 00:01:32,820
 Awesome.

62
00:01:32,820 --> 00:01:33,680
 I'm to grab--

63
00:01:33,680 --> 00:01:35,590
what do I have that
I can do this with?

64
00:01:35,590 --> 00:01:36,610
 Yes, Percy?

65
00:01:36,610 --> 00:01:38,720
 But they look like
spirits floating upwards?

66
00:01:38,720 --> 00:01:39,600
 Or do they look like bodies?

67
00:01:39,600 --> 00:01:40,558
 You watch a few more.

68
00:01:40,558 --> 00:01:44,380
You watch a couple more go by,
and they are nearly-skeletal

69
00:01:44,380 --> 00:01:45,292
bodies--

70
00:01:45,292 --> 00:01:46,000
 Falling upward.

71
00:01:46,000 --> 00:01:47,320
 --falling upward.

72
00:01:47,320 --> 00:01:50,160
But, as they watch a
couple of more go by,

73
00:01:50,160 --> 00:01:53,530
they're limp,
somewhat, but still

74
00:01:53,530 --> 00:01:55,906
moving as they scoot past.

75
00:01:55,906 --> 00:01:56,405
 Oh, no.

76
00:01:56,405 --> 00:01:57,116
 Oh, Jesus.

77
00:01:57,116 --> 00:01:58,740
 Let's just get
across here, shall we?

78
00:01:58,740 --> 00:02:00,760
 I'm going to use
divine sense now--

79
00:02:00,760 --> 00:02:01,260
 OK.

80
00:02:01,260 --> 00:02:04,130
 --just to confirm.

81
00:02:04,130 --> 00:02:05,862
 You have to wait
about a minute or so

82
00:02:05,862 --> 00:02:07,820
before one comes close
enough to hit the sense,

83
00:02:07,820 --> 00:02:11,550
but there is an undead
energy that passes past.

84
00:02:11,550 --> 00:02:14,410
And both you and Kerr
kind of have this moment

85
00:02:14,410 --> 00:02:19,491
from across the ravine from
each other seeing this.

86
00:02:19,491 --> 00:02:20,490
 I'm going to try and--

87
00:02:20,490 --> 00:02:21,498
 Kerr will throw up the horns.

88
00:02:21,498 --> 00:02:21,998
 [LAUGHS]

89
00:02:21,998 --> 00:02:25,265
 That's me to you, man.

90
00:02:25,265 --> 00:02:26,140
 Try and clock them.

91
00:02:26,140 --> 00:02:28,181
See if it's the same bodies
over and over again--

92
00:02:28,181 --> 00:02:29,760
if it's in that loop of falling.

93
00:02:29,760 --> 00:02:30,510
 Oh, interesting.

94
00:02:30,510 --> 00:02:32,450
 An infinite loop of falling?

95
00:02:32,450 --> 00:02:34,420
 OK, so I want to
get a good look at--

96
00:02:34,420 --> 00:02:34,670
 Oh, it's a gif.

97
00:02:34,670 --> 00:02:35,440
 --one of them as
they go by and see.

98
00:02:35,440 --> 00:02:35,940
 OK.

99
00:02:35,940 --> 00:02:36,579
 It's a gif.

100
00:02:36,579 --> 00:02:37,120
 It's a gif.

101
00:02:37,120 --> 00:02:38,100
Six seconds.

102
00:02:38,100 --> 00:02:39,430
So I'm just-- aw, aw!

103
00:02:39,430 --> 00:02:40,900
 Three or four
more stumble past.

104
00:02:40,900 --> 00:02:44,386
[CRACKING NOISE] And they're
getting more and more--

105
00:02:44,386 --> 00:02:45,531
 More and more frequent.

106
00:02:45,531 --> 00:02:46,030
 --rapid.

107
00:02:46,030 --> 00:02:46,240
And they're--

108
00:02:46,240 --> 00:02:47,240
 We should probably go.

109
00:02:47,240 --> 00:02:49,750
 They're different sizes.

110
00:02:49,750 --> 00:02:51,649
They do not appear to
be the same bodies.

111
00:02:51,649 --> 00:02:52,690
 Go, go, go, go, go, go.

112
00:02:52,690 --> 00:02:56,080
 I pick up Pike and try
to barrel roll across.

113
00:02:56,080 --> 00:02:56,599
 OK.

114
00:02:56,599 --> 00:02:57,140
 You got it.

115
00:02:57,140 --> 00:02:58,300
 You barrel roll across.

116
00:02:58,300 --> 00:03:00,415
As you're making it
across, three more

117
00:03:00,415 --> 00:03:03,310
come tumbling up-- two of them
too far away, but one of them

118
00:03:03,310 --> 00:03:06,476
does kind of reach out with his
arms as it tumbles past you,

119
00:03:06,476 --> 00:03:08,226
and kind of swats out
to try and grab you.

120
00:03:08,226 --> 00:03:09,745
 What?

121
00:03:09,745 --> 00:03:13,160
 I need you to make
an acrobatics check.

122
00:03:13,160 --> 00:03:13,930
 You've got this.

123
00:03:13,930 --> 00:03:15,310
Oh, you've got such a bonus.

124
00:03:15,310 --> 00:03:16,353
Come on, girl.

125
00:03:16,353 --> 00:03:17,394
 I know, I know, I know.

126
00:03:17,394 --> 00:03:20,440


127
00:03:20,440 --> 00:03:22,460
 Such a bonus, huh?

128
00:03:22,460 --> 00:03:24,620
Such a bonus.

129
00:03:24,620 --> 00:03:25,540
 Oh, no.

130
00:03:25,540 --> 00:03:26,920
Look at Matt's face.

131
00:03:26,920 --> 00:03:28,780
Look at his face.

132
00:03:28,780 --> 00:03:29,450
 Oh, fuck, no.

133
00:03:29,450 --> 00:03:29,747
 But, what?

134
00:03:29,747 --> 00:03:29,946
But he--

135
00:03:29,946 --> 00:03:30,194
 No.

136
00:03:30,194 --> 00:03:30,940
Come on.

137
00:03:30,940 --> 00:03:32,300
 Such a bonus.

138
00:03:32,300 --> 00:03:33,696
 No, come on.

139
00:03:33,696 --> 00:03:34,310
 Such a bonus

140
00:03:34,310 --> 00:03:36,034
 Did they both make that roll?

141
00:03:36,034 --> 00:03:37,450
 She's the one
driving the broom.

142
00:03:37,450 --> 00:03:38,950
She's having to try
and maneuver it.

143
00:03:38,950 --> 00:03:40,075
 She's the one in the air.

144
00:03:40,075 --> 00:03:41,650
 I was about to
interject, and I

145
00:03:41,650 --> 00:03:43,360
know that I didn't
make that fast enough.

146
00:03:43,360 --> 00:03:43,480
 No.

147
00:03:43,480 --> 00:03:44,980
 But at some point,
I would already

148
00:03:44,980 --> 00:03:46,840
like to be moving to
interject in this.

149
00:03:46,840 --> 00:03:47,620
 Right.

150
00:03:47,620 --> 00:03:49,510
As you're watching
them start across,

151
00:03:49,510 --> 00:03:51,187
you get a step out
onto the bridge--

152
00:03:51,187 --> 00:03:52,270
 I'm not going onto the--

153
00:03:52,270 --> 00:03:54,305
I'm pulling out my
gun, and trying to--

154
00:03:54,305 --> 00:03:55,180
and like, I see the--

155
00:03:55,180 --> 00:03:55,680
 Oh, wait!

156
00:03:55,680 --> 00:03:57,920
Doesn't Pike have a thing that--

157
00:03:57,920 --> 00:03:59,860
she can help people with their--

158
00:03:59,860 --> 00:04:01,990
 War god's blessing?

159
00:04:01,990 --> 00:04:03,480
 --war god's blessing?

160
00:04:03,480 --> 00:04:04,480
 That's an attack roll.

161
00:04:04,480 --> 00:04:04,916
 Damn it!

162
00:04:04,916 --> 00:04:05,707
 That's all right.

163
00:04:05,707 --> 00:04:07,540
War gods don't help you survive.

164
00:04:07,540 --> 00:04:08,620
They help you kill shit.

165
00:04:08,620 --> 00:04:08,990
 They do.

166
00:04:08,990 --> 00:04:09,730
 Do what you were going to do.

167
00:04:09,730 --> 00:04:10,000
 So--

168
00:04:10,000 --> 00:04:11,930
 I was going to start skeet
shooting at some point.

169
00:04:11,930 --> 00:04:12,805
 Well, I rolled a 1.

170
00:04:12,805 --> 00:04:14,670
 I understood, but
before we get to that--

171
00:04:14,670 --> 00:04:16,450
 Yeah, finish it.

172
00:04:16,450 --> 00:04:22,029
 So the creature grabs you,
and essentially grapples you

173
00:04:22,029 --> 00:04:22,660
in the moment.

174
00:04:22,660 --> 00:04:23,570
 Yeah.

175
00:04:23,570 --> 00:04:25,990
 And as that happens, the
momentum of pulling you

176
00:04:25,990 --> 00:04:29,110
up causes the broom to
kind of shift out of whack,

177
00:04:29,110 --> 00:04:33,100
and you begin to careen
at a slight circle.

178
00:04:33,100 --> 00:04:36,960
I need you and Pike to both
make strength saving throws.

179
00:04:36,960 --> 00:04:39,420
 Oh.

180
00:04:39,420 --> 00:04:40,930
 How far away is this from--

181
00:04:40,930 --> 00:04:41,385
 Do you want help with this?

182
00:04:41,385 --> 00:04:42,820
 They're about
30 feet from you.

183
00:04:42,820 --> 00:04:42,930
 Or do you want to do it?

184
00:04:42,930 --> 00:04:44,380
 And what about from Keyleth?

185
00:04:44,380 --> 00:04:45,182
 That's OK.

186
00:04:45,182 --> 00:04:47,140
 About the same, because
your guys are about--

187
00:04:47,140 --> 00:04:48,782
whereas they're on the
other side of the ravine.

188
00:04:48,782 --> 00:04:49,070
 You do this?

189
00:04:49,070 --> 00:04:49,695
 I'll do hers.

190
00:04:49,695 --> 00:04:51,229
 She's got a plus
4 on strength.

191
00:04:51,229 --> 00:04:52,104
 Plus 4 on strength?

192
00:04:52,104 --> 00:04:52,717
 Correct.

193
00:04:52,717 --> 00:04:53,550
 My foot is asleep.

194
00:04:53,550 --> 00:04:57,440


195
00:04:57,440 --> 00:04:58,670
 9 for me.

196
00:04:58,670 --> 00:04:59,630
 9 for you.

197
00:04:59,630 --> 00:05:01,150
 13 for Pike.

198
00:05:01,150 --> 00:05:04,200
 13 for Pike.

199
00:05:04,200 --> 00:05:10,900
So-- OK.

200
00:05:10,900 --> 00:05:12,480
So Pike's on the broom with you.

201
00:05:12,480 --> 00:05:13,680
 Yeah.

202
00:05:13,680 --> 00:05:15,900
 You get grabbed and
yanked off the broom.

203
00:05:15,900 --> 00:05:17,560
 Oh shit.

204
00:05:17,560 --> 00:05:18,060
 Pike--

205
00:05:18,060 --> 00:05:18,690
 I am tethered.

206
00:05:18,690 --> 00:05:19,940
 --manages to hold the broom.

207
00:05:19,940 --> 00:05:22,512
But currently, Pike was not
the current driver of it.

208
00:05:22,512 --> 00:05:23,970
And so she's
clutching a broom that

209
00:05:23,970 --> 00:05:25,330
is just floating to gravity.

210
00:05:25,330 --> 00:05:29,790
You watch as both
Pike and Vex go

211
00:05:29,790 --> 00:05:33,817
from careening off to tumbling
upward into the darkness above.

212
00:05:33,817 --> 00:05:35,400
 Are they both
attached to the broom?

213
00:05:35,400 --> 00:05:35,900
 No.

214
00:05:35,900 --> 00:05:37,160
Vex is pulled off the broom.

215
00:05:37,160 --> 00:05:38,502
 But she's still tethered.

216
00:05:38,502 --> 00:05:39,710
 She's still tethered, yeah.

217
00:05:39,710 --> 00:05:39,970
 Vex has a tether.

218
00:05:39,970 --> 00:05:42,131
 So both of them are trying to
like [SPUTTERING NOISES] kind

219
00:05:42,131 --> 00:05:42,510
of spinning.

220
00:05:42,510 --> 00:05:43,593
 It's a slingshot-- bola.

221
00:05:43,593 --> 00:05:45,125
 --to get your motion upward.

222
00:05:45,125 --> 00:05:45,750
 Does Keyleth have an action?

223
00:05:45,750 --> 00:05:47,708
 Right now, the two of
you can make an action.

224
00:05:47,708 --> 00:05:49,470
 All right, my first action--

225
00:05:49,470 --> 00:05:50,970
I was just pulling
to try and skeet

226
00:05:50,970 --> 00:05:54,890
shoot, and do a sharp shoot
with Bad News to hit that the--

227
00:05:54,890 --> 00:05:55,560
 The undead?

228
00:05:55,560 --> 00:05:56,410
 --undead that
had grappled her--

229
00:05:56,410 --> 00:05:56,530
 Go for it.

230
00:05:56,530 --> 00:05:57,570
 --and try and--

231
00:05:57,570 --> 00:06:02,010
I assume I have to spend a
grit to get him to let go--

232
00:06:02,010 --> 00:06:02,925
to shove him off.

233
00:06:02,925 --> 00:06:03,716
 To shove him off?

234
00:06:03,716 --> 00:06:04,980
OK.

235
00:06:04,980 --> 00:06:07,619
You are, because of the
nature of the rapid movement,

236
00:06:07,619 --> 00:06:09,160
you have disadvantage
on this attack.

237
00:06:09,160 --> 00:06:09,660
 OK.

238
00:06:09,660 --> 00:06:11,370
 You have that
inspiration, right?

239
00:06:11,370 --> 00:06:12,935
 I still have that
d12 inspiration.

240
00:06:12,935 --> 00:06:13,435
 Oh!

241
00:06:13,435 --> 00:06:15,140
Hey, can we have like
Bigby's hand go voomp?

242
00:06:15,140 --> 00:06:15,640
 Matt?

243
00:06:15,640 --> 00:06:16,170
 Yes.

244
00:06:16,170 --> 00:06:19,290
 Watching this, I'm going
to pull Whisper out, and wait

245
00:06:19,290 --> 00:06:21,690
and watch to see my sister
falling through the air.

246
00:06:21,690 --> 00:06:23,148
And if I see her
falling, I'm going

247
00:06:23,148 --> 00:06:25,001
to throw the dagger at her.

248
00:06:25,001 --> 00:06:25,500
 OK.

249
00:06:25,500 --> 00:06:26,140
She's falling.

250
00:06:26,140 --> 00:06:27,120
 Jessie?

251
00:06:27,120 --> 00:06:28,080
 Down or up?

252
00:06:28,080 --> 00:06:28,760
 Up.

253
00:06:28,760 --> 00:06:30,480
 I'm watching him,
and I'm watching her.

254
00:06:30,480 --> 00:06:31,188
And if she goes--

255
00:06:31,188 --> 00:06:31,810
 OK.

256
00:06:31,810 --> 00:06:32,730
 That doesn't--

257
00:06:32,730 --> 00:06:34,530
 How far away is she from me?

258
00:06:34,530 --> 00:06:36,480
 Right now, she's about--

259
00:06:36,480 --> 00:06:38,505
because she was up 30
feet out from the ravine,

260
00:06:38,505 --> 00:06:41,380
and now tilting upwards,
she's about 50 feet from you.

261
00:06:41,380 --> 00:06:42,199
 50 feet from me?

262
00:06:42,199 --> 00:06:42,990
 50 feet from you.

263
00:06:42,990 --> 00:06:44,160
 Because he's on the
other side, correct?

264
00:06:44,160 --> 00:06:45,200
 If you bamf out--

265
00:06:45,200 --> 00:06:45,890
 Oh, for fuck sake, really?

266
00:06:45,890 --> 00:06:46,260
 --don't do it.

267
00:06:46,260 --> 00:06:47,400
Because then you really--

268
00:06:47,400 --> 00:06:48,210
 Nothing?

269
00:06:48,210 --> 00:06:49,680
 Worse than nothing. --won't
be able to do anything.

270
00:06:49,680 --> 00:06:50,030
 Misfire?

271
00:06:50,030 --> 00:06:50,530
 Misfire.

272
00:06:50,530 --> 00:06:51,690
 That's not true.

273
00:06:51,690 --> 00:06:52,850
 Keyleth's going to go.

274
00:06:52,850 --> 00:06:56,705
Seeing Percy misfire, Keyleth is
going to grab a grasping vine--

275
00:06:56,705 --> 00:06:58,330
 Fucking cleric
right now while this--

276
00:06:58,330 --> 00:07:00,788
 --on the side of the wall,
and she's going to go for that

277
00:07:00,788 --> 00:07:01,490
broom.

278
00:07:01,490 --> 00:07:02,000
 OK.

279
00:07:02,000 --> 00:07:04,400
 Because both Pike and her are
still grabbing on to the broom,

280
00:07:04,400 --> 00:07:04,900
yeah?

281
00:07:04,900 --> 00:07:06,250
 And I'm throwing Whisper Vex.

282
00:07:06,250 --> 00:07:06,750
 OK.

283
00:07:06,750 --> 00:07:07,740
 I'm doing it.

284
00:07:07,740 --> 00:07:11,100
 The vine, unfortunately,
only has a range of 30 feet.

285
00:07:11,100 --> 00:07:13,110
 She's not-- you said
she was 30 feet away!

286
00:07:13,110 --> 00:07:15,630
 She's 30 feet out-- like
in the center of the ravine,

287
00:07:15,630 --> 00:07:18,300
and is now 15, 20
feet up in the air.

288
00:07:18,300 --> 00:07:19,590
 Can she not hit the wall--

289
00:07:19,590 --> 00:07:21,090
get a vine off the
side of the wall?

290
00:07:21,090 --> 00:07:23,280
 On the opposite side?

291
00:07:23,280 --> 00:07:24,473
Maybe.

292
00:07:24,473 --> 00:07:25,056
I am holding--

293
00:07:25,056 --> 00:07:27,260
 I've got like a 60-foot
range for this spell--

294
00:07:27,260 --> 00:07:27,440
 --the tether.

295
00:07:27,440 --> 00:07:28,913
 --plus the 30 feet from
the grasping vine, right?

296
00:07:28,913 --> 00:07:29,640
 You're holding--
oh, are they tethered?

297
00:07:29,640 --> 00:07:30,770
 Or is it 30 and 30?

298
00:07:30,770 --> 00:07:32,626
 No, we're not
tethered to that.

299
00:07:32,626 --> 00:07:33,100
 They just fucking went for it.

300
00:07:33,100 --> 00:07:33,912
 I thought you
were still grabbing

301
00:07:33,912 --> 00:07:34,860
on to the tether to the--

302
00:07:34,860 --> 00:07:35,920
 I'm tethered to the broom.

303
00:07:35,920 --> 00:07:36,660
 --to the broom.

304
00:07:36,660 --> 00:07:37,005
To the broom?

305
00:07:37,005 --> 00:07:37,537
Oh, you're tethered
to the broom?

306
00:07:37,537 --> 00:07:38,371
And Pike is still grabbing
on to the broom, yeah?

307
00:07:38,371 --> 00:07:38,790
Yeah, that's what I mean.

308
00:07:38,790 --> 00:07:40,350
Don't Whisper, because
the broom won't

309
00:07:40,350 --> 00:07:41,910
be able to support
me, you, and Pike.

310
00:07:41,910 --> 00:07:43,003
 I'm going for the broom.

311
00:07:43,003 --> 00:07:44,044
 You've roped the broom.

312
00:07:44,044 --> 00:07:44,482
All right, never mind.

313
00:07:44,482 --> 00:07:45,124
 Ah!

314
00:07:45,124 --> 00:07:45,665
Come on, man!

315
00:07:45,665 --> 00:07:48,620
 All right.

316
00:07:48,620 --> 00:07:49,829
 [LAUGHS]

317
00:07:49,829 --> 00:07:51,370
 Look, I'm trying
to work over the--

318
00:07:51,370 --> 00:07:53,537
I'm letting you guys do a
lot of things really fast.

319
00:07:53,537 --> 00:07:54,203
 That's insane.

320
00:07:54,203 --> 00:07:54,936
 (SCREAMING) Ah!

321
00:07:54,936 --> 00:07:55,280
 I'm trying--

322
00:07:55,280 --> 00:07:57,321
 I have six more moves
I want to make right now.

323
00:07:57,321 --> 00:07:58,500
 I'm working with you.

324
00:07:58,500 --> 00:08:00,420
So please bear with me on this.

325
00:08:00,420 --> 00:08:00,920
 Fuck yes.

326
00:08:00,920 --> 00:08:01,415
Yes.

327
00:08:01,415 --> 00:08:01,910
Yes.

328
00:08:01,910 --> 00:08:02,205
 Grasping vine!

329
00:08:02,205 --> 00:08:02,500
What?

330
00:08:02,500 --> 00:08:03,190
What?

331
00:08:03,190 --> 00:08:04,634
 You're like a loan officer.

332
00:08:04,634 --> 00:08:06,050
 I wouldn't
recommend doing that.

333
00:08:06,050 --> 00:08:07,290
I won't give you a
chance to even do it.

334
00:08:07,290 --> 00:08:07,690
 No lincolning.

335
00:08:07,690 --> 00:08:08,160
No lincolning.

336
00:08:08,160 --> 00:08:08,440
 Oh no!

337
00:08:08,440 --> 00:08:09,417
 Don't taunt the DM.

338
00:08:09,417 --> 00:08:11,250
 I want to sell you
this house, but there's

339
00:08:11,250 --> 00:08:12,541
some things we need to go over.

340
00:08:12,541 --> 00:08:13,310
 Shh!

341
00:08:13,310 --> 00:08:16,765
 So I'm going to
ask you, Keyleth--

342
00:08:16,765 --> 00:08:17,265
 Yeah?

343
00:08:17,265 --> 00:08:21,890
 --to go ahead and make just
a general dexterity check.

344
00:08:21,890 --> 00:08:22,830
 Oh shit.

345
00:08:22,830 --> 00:08:23,940
 Just general dex?

346
00:08:23,940 --> 00:08:26,020
 Yes.

347
00:08:26,020 --> 00:08:26,800
 What?

348
00:08:26,800 --> 00:08:29,260
 This is how fast you're
able to chant a spell, which

349
00:08:29,260 --> 00:08:34,120
requires an incantation and the
release of the spell's effect,

350
00:08:34,120 --> 00:08:36,460
thus summoning the vine
and causing it to lash out.

351
00:08:36,460 --> 00:08:38,226
This is just a base DC.

352
00:08:38,226 --> 00:08:40,059
I need you to go ahead
and just roll and add

353
00:08:40,059 --> 00:08:42,520
your dexterity modifier.

354
00:08:42,520 --> 00:08:43,120
 You got it.

355
00:08:43,120 --> 00:08:43,320
You got this.

356
00:08:43,320 --> 00:08:43,861
 I love you.

357
00:08:43,861 --> 00:08:46,360
 I love you.

358
00:08:46,360 --> 00:08:46,860
 Ooh!

359
00:08:46,860 --> 00:08:48,220
That was a natural 18.

360
00:08:48,220 --> 00:08:53,280
So that is plus my dex,
which is plus 2, so 21.

361
00:08:53,280 --> 00:08:53,780
Ha!

362
00:08:53,780 --> 00:08:54,280
 OK.

363
00:08:54,280 --> 00:08:55,176
With that, a vine--

364
00:08:55,176 --> 00:08:55,675
 Don't ha!

365
00:08:55,675 --> 00:08:56,250
Don't ha!

366
00:08:56,250 --> 00:08:56,550
Don't taunt the GM!

367
00:08:56,550 --> 00:08:56,810
 What are you doing?

368
00:08:56,810 --> 00:08:58,018
Stop rubbing it in his face--

369
00:08:58,018 --> 00:08:58,907
 Shu-- I mean, huh!

370
00:08:58,907 --> 00:08:59,740
 --for fuck's sake!

371
00:08:59,740 --> 00:09:02,700
 Huh!

372
00:09:02,700 --> 00:09:04,734
 So as Bad News misfires.

373
00:09:04,734 --> 00:09:07,150
You watch as the front of it
just [EXPLOSION NOISE] --this

374
00:09:07,150 --> 00:09:10,450
burst of black smoke
bursts outward,

375
00:09:10,450 --> 00:09:13,855
and this look of utter
horror on Percy's face--

376
00:09:13,855 --> 00:09:17,680
Keyleth rushes out, and grabbing
the edge of the end of the rope

377
00:09:17,680 --> 00:09:19,570
bridge to get enough
of a reach out there,

378
00:09:19,570 --> 00:09:21,340
you watch as her hair lifts
up and part of her body

379
00:09:21,340 --> 00:09:22,640
begins to just drift upward.

380
00:09:22,640 --> 00:09:25,780
But she holds on, and
reaches out with the staff,

381
00:09:25,780 --> 00:09:28,090
using the strength
of her force of will.

382
00:09:28,090 --> 00:09:31,114
The edge of the stone above
the ravine cracks open--

383
00:09:31,114 --> 00:09:31,780
[CRACKING NOISE]

384
00:09:31,780 --> 00:09:33,280
And suddenly, you
watch where there

385
00:09:33,280 --> 00:09:37,810
is no life, a piercing flash
of dark green shoots outward.

386
00:09:37,810 --> 00:09:41,170
A vine, freshly grown from
the edge of the stonework,

387
00:09:41,170 --> 00:09:43,480
reaches out and grabs, wrapping
around the broom itself

388
00:09:43,480 --> 00:09:44,396
and holding it taught.

389
00:09:44,396 --> 00:09:46,960
You watch as the vine and the
entire group kind of fwooshes

390
00:09:46,960 --> 00:09:48,720
upward.

391
00:09:48,720 --> 00:09:49,740
You're suddenly pulled!

392
00:09:49,740 --> 00:09:53,470
You're still grappled by this
undead zombie-like corpse.

393
00:09:53,470 --> 00:09:56,100
And Pike is currently clutching
onto the broom as hard

394
00:09:56,100 --> 00:09:59,400
as you can, as you
both impact at the end.